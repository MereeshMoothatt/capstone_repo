const express = require('express');
const router = express.Router();

const fs = require('fs');
const path = require('path');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'upload');
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});

const upload = multer({ storage: storage });

const { ensureAuthenticated } = require('../config/auth');

const Blog = require("../models/blog");

const Site = require("../models/site");

// title to slug convert
function replaceAll(string, search, replace) {
    return string.split(search).join(replace);
}

// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data 
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

/* GET home page. */
router.get('/', async function (req, res, next) {
    
    const blog = await Blog.find();
    res.render('pages/index', {
        blog: blog,
        user: req.user,
        siteSettings: res.siteSettings
    });
});

router.get('/blog-details/:blogSlug', async (req, res) => {
    const blogSlug = req.params.blogSlug;
    console.log(blogSlug);
    await Blog.findOne({ slug: blogSlug }, (err, data) => {
        if (err) {
            res.status(500).json({
                message: "Something went wrong, please try again later.",
            });
        } else {
            res.render('pages/blogDetails', {
                user: req.user,
                data: data,
                siteSettings: res.siteSettings
            });
        }
    });
});

router.get('/blog-list', ensureAuthenticated, async (req, res) => {
    const blog = await Blog.find({ users: req.user });
    res.render('pages/blogList', {
        user: req.user,
        blog: blog,
        siteSettings: res.siteSettings
    });
});

router.get('/blog/:blogSlug', ensureAuthenticated, async (req, res) => {
    const blogSlug = req.params.blogSlug;
    console.log(blogSlug);
    await Blog.findOne({ slug: blogSlug }, (err, data) => {
        if (err) {
            res.status(500).json({
                message: "Something went wrong, please try again later.",
            });
        } else {
            res.render('pages/blogEdit', {
                user: req.user,
                data: data,
                siteSettings: res.siteSettings
            });
        }
    });
});

router.get('/blog-add', ensureAuthenticated, (req, res) => {
    res.render('pages/blogAdd', {
        user: req.user,
        siteSettings: res.siteSettings
    });
});

router.post('/blog-add', ensureAuthenticated, (req, res) => {
    const blog = new Blog(req.body);

    blog.slug = replaceAll(blog.title.toLowerCase().trim(), " ", "-");
    blog.save((err, blog) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }
        res.redirect("/blog-list");
    });
});

router.post('/blog-edit/:blogId', ensureAuthenticated, async (req, res) => {
    const blogId = req.params.blogId;
    await Blog.findByIdAndUpdate({ _id: blogId }, { $set: req.body }, (err, data) => {
        if (err) {
            res.status(500).json({
                message:
                    "Something went wrong, please try again later.",
            });
        } else {
            res.redirect("/blog-list");
        }
    });
});

router.post('/blog-delete/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    await Blog.deleteOne({ _id: id }, (err, data) => {
        if (err) {
            res.status(500).json({
                message: "Something went wrong, please try again later.",
            });
        } else {
            res.redirect("/blog-list");
        }
    });
});

router.get('/site-update', ensureAuthenticated, async (req, res) => {
    await Site.findOne({}, (err, data) => {
        if (err) {
            res.status(500).json({
                message: "Something went wrong, please try again later.",
            });
        } else {
            res.render('pages/siteUpdate', {
                user: req.user,
                data: data,
                siteSettings: res.siteSettings
            });
        }
    });
});

router.post('/site-update/:siteId', upload.single('logo'), ensureAuthenticated, async (req, res, next) => {
    const siteId = req.params.siteId;
    if(req.file){
        req.body.logo = {
            data: fs.readFileSync(path.join(__dirname, '..', '/upload/', req.file.filename)),
            contentType: 'image/png'
        }
    }
    await Site.findByIdAndUpdate({ _id: siteId }, { $set: req.body }, (err, data) => {
        if (err) {
            res.status(500).json({
                message:
                    "Something went wrong, please try again later.",
            });
        } else {
            res.redirect("/blog-list");
        }
    });
});


module.exports = router; 