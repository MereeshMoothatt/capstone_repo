const mongoose = require('mongoose');
const SiteSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    logo: {
        data: Buffer,
        contentType: String
    },
    footer: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const Site = mongoose.model('Site', SiteSchema);
module.exports = Site;