const express = require('express');
const path = require('path');
const http = require("http");
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require("body-parser");

const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require("passport");

const app = express();
const Site = require("./models/site");
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'assets')));
app.use('/tinymce', express.static(path.join(__dirname, 'node_modules/tinymce')));

// local variables
const PORT = 5000;
//passport config:
require('./config/passport')(passport)
//mongoose
mongoose.connect("mongodb://localhost:27017/blogDB?readPreference=primary&appname=MongoDB%20Compass&ssl=false", { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connected....'))
  .catch((err) => console.log(err));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//express session
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(async (req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');

  await Site.findOne({}, (err, site) => {
    if (site == null) {
      const site = new Site({ title: "Simple Blog", logo: { data: "", contentType: "image/png"}, footer: "Copyrights © 2020 All Rights Reserved by Simple Blog" });
      return site.save((err, site) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        res.siteSettings = {};
        res.siteSettings.title = site.title;
        res.siteSettings.logo = site.logo;
        res.siteSettings.footer = site.footer;
        next();
      });
    }
    if (err) {
      res.status(500).json({
        message: "Something went wrong, please try again later.",
      });
      return;
    } else {
      res.siteSettings = {};
      res.siteSettings.title = site.title;
      res.siteSettings.logo = site.logo;
      res.siteSettings.footer = site.footer;
      next();
    }
  });
});

//Routes
app.use('/', require('./routes/index'));
app.use('/admin', require('./routes/users'));

// Server Create
const server = http.createServer(app);

server.listen(PORT, error => {
  if (error) {
    console.error(error);
  } else {
    console.info("Listening on port %s.", PORT);
  }
});
